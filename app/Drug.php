<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drug extends Model
{
    public function category()
    {
        return $this->belongsTo('\App\DrugCategory', 'category_id');
    }

    public function pharmacy()
    {
        return $this->belongsTo('\App\Pharmacy', 'pharmacy_id');
    }
}
