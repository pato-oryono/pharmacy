<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrugCategory extends Model
{
    public function drug()
    {
        return $this->hasMany('\App\Drug', 'category_id');
    }
}
