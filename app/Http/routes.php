<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pharmacies', 'PharmacyController@index');

Route::get('/pharmacies/{id}', 'PharmacyController@show');



