<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    public function pharmacy()
    {
        return $this->belongsTo('\App\Pharmacy','pharmacy_id');
    }

    public function service()
    {
        return $this->belongsTo('\App\Service','service_id');
    }


}
