<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pharmacy extends Model
{
    protected $fillable = [
        'name',
        'address',
        'lat',
        'lng'
    ];

    public function offers()
    {
        return $this->hasMany('\App\Offer', 'pharmacy_id');
    }

    public function doctor()
    {
        return $this->hasMany('\App\Doctor', 'pharmacy_id');
    }

    public function drugs()
    {
        return $this->hasMany(Drug::class, 'pharmacy_id');
    }
}
