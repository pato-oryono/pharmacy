<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PharmacySeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(DiseaseSeeder::class);
        $this->call(DoctorSeeder::class);
        $this->call(DrugCategorySeeder::class);
        $this->call(DrugSeeder::class);
        $this->call(OfferSeeder::class);
        $this->call(UserSeeder::class);

    }
}
