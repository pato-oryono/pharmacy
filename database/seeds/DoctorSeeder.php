<?php

use Illuminate\Database\Seeder;

class DoctorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $doctors = array(
            array(
                'firstname'=>'Charles',
                'lastname'=>'Olara',
                'email'=>'',
                'phone'=>'0312266264',
                'pharmacy_id'=>1,
                'password'=> Hash::make('doctor')

            ),

            array(
                'firstname'=>'Prosper',
                'lastname'=>'Odeke',
                'email'=>'',
                'phone'=>'0471433275',
                'pharmacy_id'=>2,
                'password'=> Hash::make('doctor')

            ),

            array(
                'firstname'=>'Simon',
                'lastname'=>'Lubanga Kene',
                'email'=>'',
                'phone'=>'0712224542',
                'pharmacy_id'=>3,
                'password'=> Hash::make('doctor')

            ),

            array(
                'firstname'=>'Jackson',
                'lastname'=>'Clark',
                'email'=>'',
                'phone'=>'0471432046',
                'pharmacy_id'=>4,
                'password'=> Hash::make('doctor')

            ),

            array(
                'firstname'=>'Simon',
                'lastname'=>'Sekitto',
                'email'=>'',
                'phone'=>'0712335566',
                'pharmacy_id'=>5,
                'password'=> Hash::make('doctor')

            ),

            array(
                'firstname'=>'Ryan',
                'lastname'=>'Kirya',
                'email'=>'',
                'phone'=>'0752374339',
                'pharmacy_id'=>6,
                'password'=> Hash::make('doctor')

            ),

            array(
                'firstname'=>'Peter',
                'lastname'=>'Ojok',
                'email'=>'',
                'phone'=>'0772589075',
                'pharmacy_id'=>7,
                'password'=> Hash::make('doctor')

            ),

            array(
                'firstname'=>'Patrick',
                'lastname'=>'Ojara',
                'email'=>'',
                'phone'=>'0772589075',
                'pharmacy_id'=>8,
                'password'=> Hash::make('doctor')

            ),









        );

        foreach($doctors as $doctor){
            \App\Doctor::create($doctor);
        }
    }
}
