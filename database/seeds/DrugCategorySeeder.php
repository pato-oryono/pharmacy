<?php

use Illuminate\Database\Seeder;

class DrugCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories= array(
            array(
                'name'=>'Analgesics',
                'description'=>'Drugs that relieve pain. There are two main types: non-narcotic analgesics for mild pain, and narcotic analgesics for severe pain'

            ),

            array(
                'name'=>'Antacids',
                'description'=>'Drugs that relieve indigestion and heartburn by neutralizing stomach acid'

            ),

            array(
                'name'=>'Antianxiety Drugs',
                'description'=>'Drugs that suppress anxiety and relax muscles (sometimes called anxiolytics, sedatives, or minor tranquilizers)'

            ),

            array(
                'name'=>'Antiarrhythmics',
                'description'=>'Drugs used to control irregularities of heartbeat'

            ),

            array(
                'name'=>'Antibacterials',
                'description'=>'Drugs used to treat infections'

            ),

            array(
                'name'=>'Antibiotics',
                'description'=>'Drugs made from naturally occurring and synthetic substances that combat bacterial infection. Some antibiotics are effective only against limited types of bacteria. Others, known as broad spectrum antibiotics, are effective against a wide range of bacteria'

            ),

            array(
                'name'=>'Anticoagulants and Thrombolytics',
                'description'=>'Anticoagulants prevent blood from clotting. Thrombolytics help dissolve and disperse blood clots and may be prescribed for patients with recent arterial or venous thrombosis'

            ),

            array(
                'name'=>'Antidepressants',
                'description'=>' There are three main groups of mood-lifting antidepressants: tricyclics, monoamine oxidase inhibitors, and selective serotonin reuptake inhibitors (SSRIs)'

            ),

            array(
                'name'=>'Antidiarrheals',
                'description'=>'Drugs used for the relief of diarrhea. Two main types of antidiarrheal preparations are simple adsorbent substances and drugs that slow down the contractions of the bowel muscles so that the contents are propelled more slowly'

            ),

            array(
                'name'=>'Antiemetics',
                'description'=>'Drugs used to treat nausea and vomiting'

            ),

            array(
                'name'=>'Antifungals',
                'description'=>'Drugs used to treat fungal infections, the most common of which affect the hair, skin, nails, or mucous membranes'

            ),

            array(
                'name'=>'Anti-Inflammatories',
                'description'=>'Drugs used to reduce inflammation - the redness, heat, swelling, and increased blood flow found in infections and in many chronic noninfective diseases such as rheumatoid arthritis and gout'

            ),

            array(
                'name'=>'Antineoplastics',
                'description'=>'Drugs used to treat cancer'
            ),

            array(
                'name'=>'Antivirals',
                'description'=>' Drugs used to treat viral infections or to provide temporary protection against infections such as influenza'
            ),

            array(
                'name'=>'Cold Cures',
                'description'=>'Although there is no drug that can cure a cold, the aches, pains, and fever that accompany a cold can be relieved by aspirin or acetaminophen often accompanied by a decongestant, antihistamine, and sometimes caffeine'
            ),

            array(
                'name'=>'Hormones',
                'description'=>'Chemicals produced naturally by the endocrine glands (thyroid, adrenal, ovary, testis, pancreas, parathyroid). In some disorders, for example, diabetes mellitus, in which too little of a particular hormone is produced, synthetic equivalents or natural hormone extracts are prescribed to restore the deficiency. Such treatment is known as hormone replacement therapy'
            ),

            array(
                'name'=>'Laxatives',
                'description'=>'Drugs that increase the frequency and ease of bowel movements, either by stimulating the bowel wall (stimulant laxative), by increasing the bulk of bowel contents (bulk laxative), or by lubricating them (stool-softeners, or bowel movement-softeners). Laxatives may be taken by mouth or directly into the lower bowel as suppositories or enemas. If laxatives are taken regularly, the bowels may ultimately become unable to work properly without them'
            ),

            array(
                'name'=>'Muscle Relaxants',
                'description'=>'Drugs that relieve muscle spasm in disorders such as backache. Antianxiety drugs (minor tranquilizers) that also have a muscle-relaxant action are used most commonly'
            ),

            array(
                'name'=>'Sex Hormones',
                'description'=>''
            ),

            array(
                'name'=>'Tranquilizer',
                'description'=>'This is a term commonly used to describe any drug that has a calming or sedative effect. However, the drugs that are sometimes called minor tranquilizers should be called antianxiety drugs, and the drugs that are sometimes called major tranquilizers should be called antipsychotics'
            ),

            array(
                'name'=>'Vitamins',
                'description'=>'Chemicals essential in small quantities for good health. Some vitamins are not manufactured by the body, but adequate quantities are present in a normal diet. People whose diets are inadequate or who have digestive tract or liver disorders may need to take supplementary vitamins'
            ),

        );

        foreach ($categories as $category){
            \App\DrugCategory::create($category);
        }
    }
}
