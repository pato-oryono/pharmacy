<?php

use Illuminate\Database\Seeder;

class DrugSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $drugs = array(
            array(
                'name'=>'Alka-seltzer',
                'description'=>'',
                'category_id'=>2,
                'pharmacy_id'=>1
            ),

            array(
                'name'=>'Milk of magnesia',
                'description'=>'',
                'category_id'=>2,
                'pharmacy_id'=>2
            ),

            array(
                'name'=>'Pepto-Bismol',
                'description'=>'',
                'category_id'=>2,
                'pharmacy_id'=>3
            ),

            array(
                'name'=>'Amoxylin',
                'description'=>'',
                'category_id'=>6,
                'pharmacy_id'=>4
            ),

            array(
                'name'=>'Amphilin',
                'description'=>'',
                'category_id'=>6,
                'pharmacy_id'=>5
            ),

            array(
                'name'=>'Azithromycin',
                'description'=>'',
                'category_id'=>6,
                'pharmacy_id'=>6
            ),

            array(
                'name'=>'Diazepam',
                'description'=>'',
                'category_id'=>3,
                'pharmacy_id'=>7
            ),

            array(
                'name'=>'Alprazolam',
                'description'=>'',
                'category_id'=>3,
                'pharmacy_id'=>8
            ),

            array(
                'name'=>'Diclofenac',
                'description'=>'',
                'category_id'=>12,
                'pharmacy_id'=>1
            ),

            array(
                'name'=>'Ibuprofen',
                'description'=>'',
                'category_id'=>12,
                'pharmacy_id'=>2
            ),

            array(
                'name'=>'Indometacin',
                'description'=>'',
                'category_id'=>12,
                'pharmacy_id'=>3
            ),

            array(
                'name'=>'Methotrexiate',
                'description'=>'',
                'category_id'=>13,
                'pharmacy_id'=>4
            ),

            array(
                'name'=>'Fluorouracil',
                'description'=>'',
                'category_id'=>13,
                'pharmacy_id'=>5
            ),

            array(
                'name'=>'Quinine',
                'description'=>'',
                'category_id'=>18,
                'pharmacy_id'=>6
            ),

            array(
                'name'=>'Orphenadrine',
                'description'=>'',
                'category_id'=>18,
                'pharmacy_id'=>7
            ),

            array(
                'name'=>'Thioxanthines',
                'description'=>'',
                'category_id'=>20,
                'pharmacy_id'=>8
            ),

            array(
                'name'=>'Clozapine',
                'description'=>'',
                'category_id'=>20,
                'pharmacy_id'=>1
            ),

            array(
                'name'=>'Loperamide',
                'description'=>'',
                'category_id'=>9,
                'pharmacy_id'=>2
            ),

            array(
                'name'=>'Atropine',
                'description'=>'',
                'category_id'=>9,
                'pharmacy_id'=>3
            ),

            array(
                'name'=>'Tioconazole',
                'description'=>'',
                'category_id'=>11,
                'pharmacy_id'=>4
            ),

            array(
                'name'=>'Miconazole',
                'description'=>'',
                'category_id'=>11,
                'pharmacy_id'=>5
            ),

            array(
                'name'=>'Prucalopride',
                'description'=>'',
                'category_id'=>17,
                'pharmacy_id'=>6
            ),

            array(
                'name'=>'Lubiprostone',
                'description'=>'',
                'category_id'=>17,
                'pharmacy_id'=>7
            ),

            array(
                'name'=>'Endocrine',
                'description'=>'',
                'category_id'=>16,
                'pharmacy_id'=>8
            ),

            array(
                'name'=>'Ketolides',
                'description'=>'',
                'category_id'=>5,
                'pharmacy_id'=>1
            ),

            array(
                'name'=>'Glycylcyclines',
                'description'=>'',
                'category_id'=>5,
                'pharmacy_id'=>2
            ),
        );

        foreach ($drugs as $drug){
            \App\Drug::create($drug);
        }
    }
}
