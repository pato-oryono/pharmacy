<?php

use Illuminate\Database\Seeder;

class OfferSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $offers = array(
            array(
                'pharmacy_id'=> 1,
                'service_id'=> 1

            ),

            array(
                'pharmacy_id'=> 1,
                'service_id'=>2

            ),

            array(
                'pharmacy_id'=>1,
                'service_id'=>4

            ),

            array(
                'pharmacy_id'=>1,
                'service_id'=>8

            ),

            array(
                'pharmacy_id'=>2,
                'service_id'=>1

            ),

            array(
                'pharmacy_id'=>2,
                'service_id'=>5

            ),

            array(
                'pharmacy_id'=>2,
                'service_id'=>9

            ),

            array(
                'pharmacy_id'=>3,
                'service_id'=>4

            ),

            array(
                'pharmacy_id'=>3,
                'service_id'=>1

            ),

            array(
                'pharmacy_id'=>3,
                'service_id'=>7

            ),

            array(
                'pharmacy_id'=>4,
                'service_id'=>1

            ),

            array(
                'pharmacy_id'=>4,
                'service_id'=>6

            ),

            array(
                'pharmacy_id'=>4,
                'service_id'=>5

            ),

            array(
                'pharmacy_id'=>4,
                'service_id'=>8

            ),

            array(
                'pharmacy_id'=>5,
                'service_id'=>1

            ),

            array(
                'pharmacy_id'=>5,
                'service_id'=>2

            ),

            array(
                'pharmacy_id'=>5,
                'service_id'=>5

            ),

            array(
                'pharmacy_id'=>5,
                'service_id'=>3

            ),

            array(
                'pharmacy_id'=>5,
                'service_id'=>7

            ),

            array(
                'pharmacy_id'=>6,
                'service_id'=>1

            ),

            array(
                'pharmacy_id'=>6,
                'service_id'=>2

            ),

            array(
                'pharmacy_id'=>6,
                'service_id'=>4

            ),

            array(
                'pharmacy_id'=>6,
                'service_id'=>6

            ),

            array(
                'pharmacy_id'=>6,
                'service_id'=>8

            ),

            array(
                'pharmacy_id'=>7,
                'service_id'=>9

            ),

            array(
                'pharmacy_id'=>8,
                'service_id'=>1

            ),

            array(
                'pharmacy_id'=>8,
                'service_id'=>3

            ),

            array(
                'pharmacy_id'=>8,
                'service_id'=>6

            ),

            array(
                'pharmacy_id'=>8,
                'service_id'=>9

            ),

            array(
                'pharmacy_id'=>8,
                'service_id'=>6

            ),

            array(
                'pharmacy_id'=>6,
                'service_id'=>3

            )
        );

        foreach ($offers as $offer){
            \App\Offer::create($offer);
        }
    }
}
