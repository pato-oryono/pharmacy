<?php

use Illuminate\Database\Seeder;

class PharmacySeeder extends Seeder
{
    public function run()
    {
        $pharmacies = array(
            array(
                'name'=>'Community Pharmacy - AAM Franchise Pharmacy',
                'address'=>'2/2 Awere Road, Gulu , Gulu',
                'lat'=>2.7714951,
                'lng'=>32.2988311
            ),

            array(
                'name'=>'Fitzmann Medical & Dental Clinic',
                'address'=>'3 Olya Road Near Mega FM, 1287,Gulu, Gulu , Gulu',
                'lat'=>2.7712635,
                'lng'=>32.3000327
            ),

            array(
                'name'=>'Gulu Pharmacy (U) Ltd.',
                'address'=>'12 Olya Road Opp. Mega FM, Gulu , Gulu',
                'lat'=>2.7736669,
                'lng'=>32.2977587
            ),

            array(
                'name'=>'Kakanyero Enterprises Pharmaceuticals',
                'address'=>'17 Bank Lane, Gulu , Gulu',
                'lat'=>2.7739452,
                'lng'=>32.297108
            ),

            array(
                'name'=>'Medizone Pharmacy Ltd.',
                'address'=>'2 Andrew Olar Road - Main Street, Gulu , Gulu',
                'lat'=>2.7423134,
                'lng'=>32.5837735
            ),

            array(
                'name'=>'Plus Pharmacy Ltd.',
                'address'=>'14 Labwor Road, Gulu , Gulu',
                'lat'=>2.7702584,
                'lng'=>32.300121
            ),

            array(
                'name'=>'Yogi Enterprises Ltd. - Pharmacy',
                'address'=>'13 Jomo Kenyata Road, Gulu , Gulu',
                'lat'=>2.7736418,
                'lng'=>32.3116257
            ),

            array(
                'name'=>'Acayo Foundation Pharmacy',
                'address'=>'Queens Avenue Nic Building (Annex), P.O.Box 600, Gulu',
                'lat'=>2.7736418,
                'lng'=>32.3116257
            ),

        );

        foreach ($pharmacies as $pharmacy){
            \App\Pharmacy::create($pharmacy);
        }
    }
}
