<?php

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services =array(
            array(
                'name' => 'Compliance aids',
                'description'=>'These aids help you remember if and when you have taken your medication, and they come in various forms – some even indicate the time and day you should be taking your medication'
            ),
            array(
                'name'=>'Influenza vaccinations',
                'description'=>'A trained pharmacist can administer the annual flu vaccine to anyone five years of age and older at participating pharmacies in the Universal Influenza Immunization Program'
            ),

            array(
                'name'=>'MedsCheck',
                'description'=>'If you are taking three or more chronic medications, have diabetes, are a resident at a long-term care home, or are physically unable to get in for a visit to your pharmacy, your pharmacist may suggest a MedsCheck appointment – free of charge to those with a valid OHIP card – to review your prescription and non-prescription medications in detail'
            ),

            array(
                'name'=>'Compounding prescriptions',
                'description'=>'If your medications are not commercially available in the dose or form they were prescribed, your pharmacist may be able to mix these medications for you'
            ),

            array(
                'name'=>'Smoking cessation support',
                'description'=>'In addition to recommending over-the-counter medications to help you quit smoking, your pharmacist can prescribe certain medications for smoking cessation. Some patients can also participate in the pharmacy smoking cessation program, which offers smoking cessation counselling for eligible Ontarians. Talk to your pharmacist to see if you qualify'
            ),

            array(
                'name'=>'Clinic days',
                'description'=>'To help you learn about specific health conditions such as heart disease or diabetes, your pharmacy may hold specialized clinic days that focus on education or risk assessment'
            ),

            array(
                'name'=>'Medication delivery',
                'description'=>'Unable to pick up your prescription? Some pharmacies offer medication delivery to patients who live within the pharmacy’s medication delivery zone'
            ),

            array(
                'name'=>'Counselling services',
                'description'=>'Your pharmacist can provide a range of advice and education about prescriptions, over-the-counter medications, natural health products, medical devices, and more. Ask your pharmacist!'
            ),

            array(
                'name'=>'Disease Testing',
                'description'=>'Testing Diseases.'
            )
        );

        foreach($services as $service){
            \App\Service::create($service);
        }
    }
}
