<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
            array(
                'firstname'=>'Aisha',
                'lastname'=>'Namugombe',
                'email'=>'baisha2168@gmail.com',
                'phone'=>'0772364534',
                'pharmacy_id'=>1,
                'password'=> Hash::make('doctor')

            ),

            array(
                'firstname'=>'Samuel',
                'lastname'=>'Okiya',
                'email'=>'okiyas@gmail.com',
                'phone'=>'0786523456',
                'pharmacy_id'=>2,
                'password'=> Hash::make('doctor')

            ),

            array(
                'firstname'=>'Simon',
                'lastname'=>'Okell0',
                'email'=>'okellos@yahoo.com',
                'phone'=>'0794234532',
                'pharmacy_id'=>3,
                'password'=> Hash::make('doctor')

            ),

            array(
                'firstname'=>'Badru',
                'lastname'=>'Kigundu',
                'email'=>'kigundus@ec.go.ug',
                'phone'=>'0794352348',
                'pharmacy_id'=>4,
                'password'=> Hash::make('doctor')

            ),

            array(
                'firstname'=>'Simon',
                'lastname'=>'Lukyamuzi',
                'email'=>'luksam@gmail.com',
                'phone'=>'0723456346',
                'pharmacy_id'=>5,
                'password'=> Hash::make('doctor')

            ),

            array(
                'firstname'=>'Stella',
                'lastname'=>'Nyanzi',
                'email'=>'stellan@muk.ac.ug',
                'phone'=>'0772342321',
                'pharmacy_id'=>6,
                'password'=> Hash::make('doctor')

            ),

            array(
                'firstname'=>'Ayoma',
                'lastname'=>'Moses',
                'email'=>'ayoma@gmail.com',
                'phone'=>'0756434234',
                'pharmacy_id'=>7,
                'password'=> Hash::make('doctor')

            ),

            array(
                'firstname'=>'Patrick',
                'lastname'=>'Oryono',
                'email'=>'me@patrickoryono.co',
                'phone'=>'0784275529',
                'pharmacy_id'=>8,
                'password'=> Hash::make('doctor')

            ),
        );

        foreach($users as $user){
            \App\User::create($user);
        }
    }
}
